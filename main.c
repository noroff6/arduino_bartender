#include <Stepper.h>
#include <LiquidCrystal.h>
//MUSIC
int buzzer = 22;

// Joystick
#define VERT_PIN A0
#define HORZ_PIN A1
#define SEL_PIN  52
int x = 0;
int y = 0;

int lightBlueLED = 46;
int greenLED = 48;
int purpleLED = 50;

bool running = true;

int ledArray[] = {lightBlueLED, greenLED, purpleLED};


LiquidCrystal lcd(13, 12, 11, 10, 9, 8);
// Frequency notes
float B = 493.88;
float C = 523.25;
float D = 587.33;
float A = 440.00;
float G = 392.00;
float E = 659.25;
float F = 698.46;
float D_low = 293.66;
float G_high = 783.99;

// Menu
int menu_chooser = 0;
int sub_category = 0;

// Drinks
String whiteRussian = "White Russian";
String whiteRussianIng[] = {"vodka", "kahlua", "milk"};

String spritzer = "Spritzer";
String spritzerIng[] = {"wine", "soda", "lime"};

String boulevardier = "Boulevardier";
String boulevardierIng[] = {"whiskey", "campari", "vermouth"};

// Songs
const float ode_to_joy[] = {B,B,C,D,D,C,B,A,G,G,A,B,B,A,A,B,B,C,D,D,C,B,A,
G,G,A,B,A,G,G,A,A,B,G,A,B,C,B,G,A,B,C,B,A,G,A,D_low,B,B,C,D,D,C,B,A,
G,G,A,B,A,G};

const float jingle_bells[] = {E,E,E,E,E,E,E,G_high,C,D,E,F,F,F,F,F,E,E,E,G,G,F,D,C};

// Intervals
const int ode_to_joy_interval[] = {500, 500, 500, 500,500, 500, 500, 500,
500, 500, 500, 500,1000, 500, 250,500, 500, 500, 500,500, 500, 500, 
500,500, 500, 500, 500,1000, 500, 250,500, 500, 500, 500, 500, 
500, 500, 500, 500, 500, 500, 500, 500, 500, 250,500, 500, 500, 
500,500, 500, 500, 500,500, 500, 500, 500,1000, 500, 250};

const int jingle_bells_interval[] = {500, 500, 500, 500,500, 500, 500, 500,
500, 500, 500, 500,500, 500, 500, 500,500, 500, 500, 500,500, 500, 500, 500};

// Size of array
int ode_to_joy_length = sizeof(ode_to_joy) / sizeof(float);
int jingle_bells_length = sizeof(jingle_bells) / sizeof(float);

// Motors
const int stepsPerRevolution = 200;
Stepper motorOne(stepsPerRevolution, 2,3);
Stepper motorTwo(stepsPerRevolution, 4,5);
Stepper motorThree(stepsPerRevolution, 6,7);

// States
const byte startUp = 0;
const byte discoLights = 1;
const byte playMusic = 2;
const byte drinks = 3;
const byte menuDown = 4;
const byte menuUp = 5;
const byte subMenuDrinks = 6;
int state = startUp;


void setup() {
  Serial.begin(9600);

  motorOne.setSpeed(60);
  motorTwo.setSpeed(60);
  motorThree.setSpeed(60);
  pinMode(buzzer, OUTPUT);

  lcd.begin(20, 4);
  state = startUp;

  // Joystick
  pinMode(VERT_PIN, INPUT);
  pinMode(HORZ_PIN, INPUT);
  pinMode(SEL_PIN, INPUT_PULLUP);
}

void loop() {
  int horz = analogRead(HORZ_PIN);
  int vert = analogRead(VERT_PIN);
  delay(2000);

  // Menu down
  if(horz == 512 && vert == 0){
    state = menuDown;
  }

  // Menu up
  if(horz == 512 && vert == 1023){
    state = menuUp;
  }

  if(menu_chooser == 1 && sub_category == 0 && digitalRead(SEL_PIN) == 0){
    state = subMenuDrinks;
  }

  if(menu_chooser == 2 && sub_category == 0 && digitalRead(SEL_PIN) == 0){
    state = playMusic;
  }

  if(menu_chooser == 3 && sub_category == 0  && digitalRead(SEL_PIN) == 0){
    state = discoLights;
    
  }

  if(menu_chooser == 1 && sub_category == 1 && digitalRead(SEL_PIN) == 1){
    state = drinks;
  }

 // STATES
  switch(state){
  case startUp: 
  welcomeMessage();
  break;

  case discoLights:
  flashLED();
  break;

  case playMusic:
  makemusic(ode_to_joy_length, ode_to_joy, ode_to_joy_interval);
  state = startUp;
  break;

  case drinks:
  if(menu_chooser == 1 && sub_category == 1)
  {
    makeDrinks(whiteRussian, whiteRussianIng);
  }
  if(menu_chooser == 2 && sub_category == 1){
    makeDrinks(spritzer, spritzerIng);
  }
  if(menu_chooser == 3 && sub_category == 1)
  {
    makeDrinks(boulevardier, boulevardierIng);
  }
  
  break;

  case menuDown:
  scrollMenuDown();
  if(sub_category == 1){
    state = subMenuDrinks;
  }
  else{
    state = startUp;
  }
  break;

  case menuUp:
  scrollMenuUp();
  if(sub_category == 1){
    state = subMenuDrinks;
  }
  else{
    state = startUp;
  }
  
  break;

  case subMenuDrinks:
  drinkMenu();
  break;
  }
  
}

void scrollMenuDown(){
  if(menu_chooser != 3)
  {
    menu_chooser = 1 + menu_chooser%3;
    lcd.noBlink();
    delay(2000);
    lcd.setCursor(1, menu_chooser);
    lcd.blink();
    delay(2000);
  }
    
}

void scrollMenuUp(){
  if(menu_chooser !=1)
  {
    menu_chooser = 1 - menu_chooser%2;
    lcd.noBlink();
    delay(2000);
    lcd.setCursor(1, menu_chooser);
    lcd.blink();
    delay(2000);
  }
}

void makemusic(int songLength, float song[], int interval[]){
  menu_chooser = 0;
  
  while(running){
   for (int i = 0; i < songLength; i++)
  {
    const int currentNote = song[i];
    if(digitalRead(SEL_PIN) == 0){
    noTone(buzzer);
    running = false;
    state = startUp;
    }
    else if (currentNote)
    {
      tone(buzzer, song[i], interval[i]);
    }
    else
    {
      noTone(buzzer);
    }
    delay(600);
  }
}
}

void welcomeMessage(){
  lcd.noBlink();
  lcd.setCursor(1, 0);
  lcd.print("Welcome to HAL2022!");
  lcd.setCursor(2, 1);
  lcd.print("1. Order drink");
  lcd.setCursor(2, 2);
  lcd.print("2. Listen to music");
  lcd.setCursor(2, 3);
  lcd.print("3. Go disco!");
}

void drinkMenu(){
  //menu_chooser = 0;
  lcd.noBlink();
  sub_category = 1;
  lcd.clear();
  lcd.setCursor(1, 0);
  lcd.print("List of Drinks!");
  lcd.setCursor(2, 1);
  lcd.print("1. White Russian!");
  lcd.setCursor(2, 2);
  lcd.print("2. Spritzer!");
  lcd.setCursor(2, 3);
  lcd.print("3. Boulevardier!");
}

void flashLED(){
  lcd.clear();
  menu_chooser = 0;
  while(running){
  int randomColor = random(0, 3);
  digitalWrite(ledArray[randomColor], HIGH);   
  delay(500);
  digitalWrite(ledArray[randomColor], LOW);
  if(digitalRead(SEL_PIN) == 0){
    running = false;
    state = startUp;
  }
  lcd.setCursor(1, 0);
  lcd.print("DISCO TIME!");
}
}

void makeDrinks(String drinkName, String drinkIng[]){
  lcd.noBlink();
  sub_category = 0;
  menu_chooser = 0; 
  lcd.clear();
  lcd.print("YOUR ORDER: ");
  lcd.setCursor(2, 1);
  lcd.print(drinkName);
  delay(3000);
  int i = 0;
  
  while(running)
  {
    if(i == 0)
    {
      lcd.clear();
      lcd.setCursor(2, 0);
      lcd.print("Adding " +drinkIng[i]);
      motorOne.step(stepsPerRevolution);
    }
   if(i == 1)
    {
      lcd.clear();
      lcd.setCursor(2, 0);
      lcd.print("Adding " +drinkIng[i]);
      motorTwo.step(stepsPerRevolution);
    }
    if(i == 2)
    {
      lcd.clear();
      lcd.setCursor(2, 0);
      lcd.print("Adding " +drinkIng[i]);
      motorThree.step(stepsPerRevolution);
      lcd.setCursor(2, 0);
      lcd.print("Enjoy your drink! ");
      running = false;
      state = startUp;
    }
    i++;
  }
}   
