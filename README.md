# Arduino Virtual Bartender "HAL 2022"
Created using [Wokwi](https://wokwi.com/). Try out the simulation [here](https://wokwi.com/projects/344801804484084306).


## Description
This project is an assignment from [Noroff](https://www.noroff.no/en/), as part of [Experis Academy for Embedded Development](https://www.experis.se/sv/it-tjanster/experis-academy). 
<br />

## Table of Contents
- [Image](#image)
- [Assignment](#assignment)
- [Implementation](#implementation)
- [Contributors](#contributors)


## Image
![](image.PNG)


## Implementation
Implemented using Wokwi and C/C++. 


## Contributors
This is a project created by [Helena Barmer](https://gitlab.com/helenabarmer) and [Joel Fredin](https://gitlab.com/joelfredin).
For any questions reach out to us.